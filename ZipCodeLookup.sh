#!/bin/bash

#url for zip codes
URL="http://www.city-data.com/zips"

#echo statement that returns zip code location
echo -n "The ZIP code $1 is located in "

#commands to isolate location for return statement
curl -s -dump "$URL/$1.html" | \
 grep -i '<title>' | \
 cut -d\( -f2 | cut -d\) -f1
exit 0
